import Vue from 'vue'
import App from './App.vue'
import Clock from 'vue-reactive-clock'
import VueHotkey from 'v-hotkey'
 
Vue.use(VueHotkey)
Vue.use(Clock)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
